from setuptools import setup

import os

NAMESPACE = 'openvpp'
PACKAGE = 'chpsim'

setup(
    author='Martin Tröschel',
    author_email='martin.troeschel@gmail.com',
    description='A simulation model of a combined heat and power plant.',
    entry_points={
        'console_scripts': [
            'mosaik-chpsim = openvpp.chpsim.mosaik:main',
        ],
    },
    include_package_data=True,
    install_requires=[
        'arrow>=0.5.4',
        'mosaik-api>=2.1',
        'numpy>=1.8',
    ],
    long_description=(
            open('README.md').read()
            # + '\n\n' +
            # open('CHANGES.txt').read() + '\n\n' +
            # open('AUTHORS.txt').read()
    ),
    name=PACKAGE,
    namespace_packages=[
        NAMESPACE,
    ],
    packages=[NAMESPACE, NAMESPACE + os.path.sep + PACKAGE],
    package_dir={'': '.'},
    setup_requires=["pytest-runner"],
    tests_require=["pytest"],
    url='https://gitlab.com/OpenVPP/ChpSim',
    version='1.0.0',
    zip_safe=False,
)
